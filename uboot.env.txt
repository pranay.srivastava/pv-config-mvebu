arch=arm
baudrate=115200
baseargs=root=/dev/ram rootfstype=ramfs init=/init
bootdelay=10
bootenv=uEnv.txt
cpu=armv7
envloadaddr=0x00200000
loadfdt=fdt addr 0x02000000; fdt get value args /chosen bootargs
loadaddr=0x01000000
rd_addr=0x02100000
loadargs=setenv bootargs "${baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${args} ${localargs}"
loadenv_mmc=load mmc ${mmcdev}:${mmcdata} ${envloadaddr} /boot/uboot.txt;
loadenv_ubi=ubifsload ${envloadaddr} /boot/uboot.txt;
loadenv=if test "${pv_fstype}" = "ubi"; then loadenv_ubi; else run loadenv_mmc; fi; setenv pv_try; env import ${envloadaddr} 0x400; if env exists pv_try; then if env exists pv_trying && test ${pv_trying} = ${pv_try}; then setenv pv_trying; saveenv; setenv boot_rev ${pv_rev}; else setenv pv_trying ${pv_try}; saveenv; setenv boot_rev ${pv_trying}; fi; else setenv boot_rev ${pv_rev}; fi;
loadstep_mmc=run loadenv; load mmc ${mmcdev}:${mmcdata} ${loadaddr} /trails/${boot_rev}/.pv/pv-kernel.img; load mmc ${mmcdev}:${mmcdata} ${rd_addr} /trails/${boot_rev}/.pv/pv-initrd.img; setenv ${rd_size} ${filesize};
loadstep_ubi=ubi part ${pv_ubipart}; ubifsmount ubi0:${pv_ubipart}; run loadenv; ubifsload ${loadaddr} /trails/${boot_rev}/.pv/pv-kernel.img; ubifsload ${rd_addr} /trails/${boot_rev}/.pv/pv-initrd.img;
loadstep=if test "${pv_fstype}" = "ubi"; then loadstep_ubi; else run loadstep_mmc; fi; setenv rd_size ${filesize}
pvbootcmd=run loadstep; run loadfdt; run loadargs; bootm ${loadaddr} ${rd_addr}:${rd_size} ${fdtaddr}; echo "Failed to boot step, rebooting"; sleep 1; reset
bootcmd=run pvbootcmd
pv_fstype=ubi
pv_ubipart=0
